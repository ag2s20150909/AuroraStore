package com.aurora.store.data.network.cronet



import android.os.ConditionVariable
import okhttp3.*
import org.chromium.net.UrlRequest
import java.io.IOException

class OldCallback(
    request: Request,
    call: Call,
    eventListener: EventListener? = null,
    responseCallback: Callback? = null
) : AbsCallBack(
    request, call, eventListener, responseCallback
) {
    private val mResponseCondition = ConditionVariable()

    private var mException: IOException? = null


    @Throws(IOException::class)
    override fun waitForDone(urlRequest: UrlRequest): Response {
        urlRequest.start()
        val timeOutMs: Long = mCall.timeout().timeoutNanos() / 1000000
        if (timeOutMs > 0) {
            mResponseCondition.block(timeOutMs)
        } else {
            mResponseCondition.block()
        }
        if (!urlRequest.isDone) {
            urlRequest.cancel()
            mException = IOException("time out")
        }
        if (mException != null) {
            throw mException!!.asIOException()
        }
        return mResponse
    }


    /**
     * 通知子类出错了，结束阻塞
     *
     * @param error
     */
    override fun onError(error: IOException) {
        mException=error
       mResponseCondition.open()
    }

    override fun onSuccess(response: Response) {
        mResponseCondition.open()
    }

    override fun close() {
        super.close()
    }


}