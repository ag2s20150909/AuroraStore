package com.aurora.store.data.network.glide

import com.aurora.store.data.network.cronet.CronetHelper.cronetEngine
import com.bumptech.glide.Priority
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.MultiModelLoaderFactory
import org.chromium.net.CronetEngine
import org.chromium.net.UrlRequest
import java.nio.ByteBuffer
import java.util.*

class CronetUrlLoaderFactory<T : Any> internal constructor() : ModelLoaderFactory<T, ByteBuffer> {
    private val engine: CronetEngine

    companion object {
        val GLIDE_TO_CHROMIUM_PRIORITY: MutableMap<Priority, Int> = EnumMap(
            Priority::class.java
        )

        init {
            GLIDE_TO_CHROMIUM_PRIORITY[Priority.IMMEDIATE] =
                UrlRequest.Builder.REQUEST_PRIORITY_HIGHEST
            GLIDE_TO_CHROMIUM_PRIORITY[Priority.HIGH] = UrlRequest.Builder.REQUEST_PRIORITY_MEDIUM
            GLIDE_TO_CHROMIUM_PRIORITY[Priority.NORMAL] = UrlRequest.Builder.REQUEST_PRIORITY_LOW
            GLIDE_TO_CHROMIUM_PRIORITY[Priority.LOW] =
                UrlRequest.Builder.REQUEST_PRIORITY_LOWEST
        }
    }

    override fun build(multiModelLoaderFactory: MultiModelLoaderFactory): ModelLoader<T, ByteBuffer> {
        return CronetUrlLoader<T, ByteBuffer>(engine)
    }

    override fun teardown() {}

    init {
        engine = cronetEngine
    }
}