package com.aurora.store.data.network.cronet


import android.util.Log
import com.aurora.store.BuildConfig
import java.io.IOException


inline fun Throwable.asIOException(): IOException {
    return IOException(message, cause)
}
fun String.print(){
    if (BuildConfig.DEBUG){
        Log.e("String",this)
    }
}








