package com.aurora.store.data.network.cronet

import android.os.Build
import androidx.annotation.RequiresApi
import okhttp3.*
import org.chromium.net.UrlRequest
import java.io.IOException
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

@RequiresApi(api = Build.VERSION_CODES.N)
class NewCallBack(
    request: Request,
    call: Call,
    eventListener: EventListener? = null,
    responseCallback: Callback? = null
) :
    AbsCallBack(request, call, eventListener, responseCallback) {


    private val responseFuture = CompletableFuture<Response>()

    @Throws(IOException::class)
    override fun waitForDone(urlRequest: UrlRequest): Response {



        urlRequest.start()
        return try {
            if (mCall.timeout().timeoutNanos() > 0) {
                responseFuture[mCall.timeout().timeoutNanos(), TimeUnit.NANOSECONDS]
            } else {
                responseFuture.get()
            }
        } catch (e: Exception) {
            throw e.asIOException()
        }
    }

    /**
     * 通知子类出错了，结束阻塞
     *
     * @param error
     */
    override fun onError(error: IOException) {
        responseFuture.completeExceptionally(error)
    }

    override fun onSuccess(response: Response) {
        responseFuture.complete(response)
    }



}