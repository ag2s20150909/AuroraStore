package com.aurora.store.data.network.cronet

import com.aurora.store.BuildConfig
import com.aurora.store.app
import com.aurora.store.util.Log
import okhttp3.CookieJar
import okhttp3.Request
import okio.Buffer
import org.chromium.net.*
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.Executor
import java.util.concurrent.Executors


object CronetHelper {


    private val executor: Executor = Executors.newCachedThreadPool()


    val cronetEngine: ExperimentalCronetEngine by lazy {
        val builder = ExperimentalCronetEngine.Builder(app.applicationContext).apply {
            setStoragePath(app.applicationContext.externalCacheDir?.absolutePath)//设置缓存路径
            enableHttpCache(
                CronetEngine.Builder.HTTP_CACHE_DISK,
                (1024 * 1024 * 50).toLong()
            )//disk cache
            enableQuic(true)//http/3
            enableHttp2(true)  //http/2
            enableBrotli(true)//Brotli
            addQuicHint("android.clients.google.com", 443, 443)
            addQuicHint("play-lh.googleusercontent.com", 443, 443)
            addQuicHint("www.bestappsale.com", 443, 443)
        }
        builder.build().also {
            //URL.setURLStreamHandlerFactory(it.createURLStreamHandlerFactory())
            if(BuildConfig.DEBUG){
                it.addRequestFinishedListener(object : RequestFinishedInfo.Listener(executor){

                    override fun onRequestFinished(requestInfo: RequestFinishedInfo?) {
                        Log.e("Protocol:${requestInfo?.responseInfo?.negotiatedProtocol} Code:${requestInfo?.responseInfo?.httpStatusCode} \r\nURL:${requestInfo?.responseInfo?.url}")
                    }
                })
            }

        }
    }




    val cronetInterceptor: CronetInterceptor by lazy { CronetInterceptor(CookieJar.NO_COOKIES) }


    @Throws(IOException::class)
    fun buildRequest(request: Request, callback: UrlRequest.Callback?): UrlRequest {
        val url = request.url.toString()
        val requestBuilder: UrlRequest.Builder =
            cronetEngine.newUrlRequestBuilder(url, callback, executor)
        requestBuilder.setHttpMethod(request.method)
        requestBuilder.allowDirectExecutor()
        requestBuilder.setPriority(UrlRequest.Builder.REQUEST_PRIORITY_IDLE)
        val headers = request.headers
        var i = 0
        while (i < headers.size) {
            requestBuilder.addHeader(headers.name(i), headers.value(i))
            i += 1
        }
        val requestBody = request.body
        if (requestBody != null) {
            requestBuilder.allowDirectExecutor()
            val contentType = requestBody.contentType()
            if (contentType != null) {
                requestBuilder.addHeader("Content-Type", contentType.toString())
            } else {
                requestBuilder.addHeader("Content-Type", "application/octet-stream")
            }
            val buffer = Buffer()
            requestBody.writeTo(buffer)
            requestBuilder.setUploadDataProvider(
                UploadDataProviders.create(buffer.readByteArray()),
                executor
            )
        }
        return requestBuilder.build()
    }

    fun openConnection(httpUrl: URL?): HttpURLConnection? {
        try {
            return cronetEngine.openConnection(httpUrl) as HttpURLConnection
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }


}
