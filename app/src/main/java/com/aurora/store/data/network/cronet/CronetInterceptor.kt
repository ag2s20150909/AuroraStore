package com.aurora.store.data.network.cronet


import android.os.Build
import okhttp3.*
import java.io.IOException


class CronetInterceptor(private val cookieJar: CookieJar = CookieJar.NO_COOKIES) : Interceptor {


    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (chain.call().isCanceled()) {
            throw IOException("Canceled")
        }

        val original: Request = chain.request()
        val builder: Request.Builder = original.newBuilder()
        builder.removeHeader("Keep-Alive")
        builder.removeHeader("Accept-Encoding")
        if (cookieJar != CookieJar.NO_COOKIES) {
            val cookieStr = getCookie(original.url)
            //set Cookie
            if (cookieStr.length > 3) {
                builder.addHeader("Cookie", cookieStr)
            }
        }

        val newReq: Request = builder.build()
        return run {
            proceedWithCronet(newReq, chain.call()).also {
                cookieJar.saveFromResponse(newReq.url, Cookie.parseAll(newReq.url, it.headers))
            }
        }
    }


    private fun proceedWithCronet(request: Request, call: Call): Response {

        //val asyncOperation = asyncOperation()
        // asyncOperation.get()

        val callBack = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            NewCallBack(request, call)
        } else {
            OldCallback(request, call)
        }

        try {
            val urlRequest = CronetHelper.buildRequest(request, callBack)
            return callBack.waitForDone(urlRequest)
        } catch (e: IOException) {
            throw e
        }

    }

    /** Returns a 'Cookie' HTTP request header with all cookies, like `a=b; c=d`. */
    private fun getCookie(url: HttpUrl): String = buildString {
        val cookies = cookieJar.loadForRequest(url)
        cookies.forEachIndexed { index, cookie ->
            if (index > 0) append("; ")
            append(cookie.name).append('=').append(cookie.value)
        }
    }

}