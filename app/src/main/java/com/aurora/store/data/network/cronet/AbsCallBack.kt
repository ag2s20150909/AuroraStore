package com.aurora.store.data.network.cronet

import android.util.Log
import okhttp3.*
import okhttp3.EventListener
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody.Companion.toResponseBody
import okio.Buffer
import org.chromium.net.CronetException
import org.chromium.net.UrlRequest
import org.chromium.net.UrlResponseInfo
import java.io.IOException
import java.nio.ByteBuffer
import java.util.*

abstract class AbsCallBack(
    private val mRequest: Request,
    protected val mCall: Call,
    private val eventListener: EventListener? = null,
    private val responseCallback: Callback? = null
) : UrlRequest.Callback(),AutoCloseable {

    private val mBuffer = Buffer()

    //@JvmField
    @Volatile
    protected var mResponse: Response

    @Volatile
    private var followCount = 0




    @Throws(IOException::class)
    abstract fun waitForDone(urlRequest: UrlRequest): Response


    /**
     * 通知子类出错了，结束阻塞
     *
     * @param error
     */
    abstract fun onError(error:IOException)
    abstract fun onSuccess(response: Response)



    @Throws(IOException::class)
    override fun onRedirectReceived(
        request: UrlRequest,
        info: UrlResponseInfo,
        newLocationUrl: String
    ) {
        if (mCall.isCanceled()) {
            request.cancel()
            onError(IOException("Request Canceled"))
        }
        if (followCount > MAX_FOLLOW_COUNT) {
            request.cancel()

            onError(IOException("Too many redirect"))
        }
        followCount += 1
        request.followRedirect()
    }


    override fun onResponseStarted(request: UrlRequest, info: UrlResponseInfo) {
        mResponse = responseFromResponse(mCall,mResponse, info)
        if (mCall.isCanceled()) {
            onError(IOException("Request Canceled"))
            request.cancel()
        } else {
            if (eventListener != null) {
                eventListener.responseHeadersEnd(mCall, mResponse)
                eventListener.responseBodyStart(mCall)
            }
            request.read(ByteBuffer.allocateDirect(64 * 1024))
        }

    }

    override fun onReadCompleted(
        request: UrlRequest,
        info: UrlResponseInfo,
        byteBuffer: ByteBuffer
    ) {
        //Log.e(TAG, info.getNegotiatedProtocol());
        byteBuffer.flip()
        try {
            mBuffer.write(byteBuffer)
        } catch (e: IOException) {
            Log.i(TAG, "IOException during ByteBuffer read. Details: ", e)
            onError(e)
        }
        byteBuffer.clear()
        request.read(byteBuffer)
    }

    override fun onSucceeded(request: UrlRequest, info: UrlResponseInfo) {
        //Log.e(TAG, info.getNegotiatedProtocol());
        eventListener?.responseBodyEnd(mCall, info.receivedByteCount)
        val contentTypeString = mResponse.header("content-type")
        val contentType: MediaType =
            (contentTypeString ?: "text/plain; charset=\"utf-8\"").toMediaType()
        val bytes = mBuffer.readByteString()
        mResponse = mResponse.newBuilder().body(bytes.toResponseBody(contentType))
            .request(mRequest.newBuilder().url(info.url).build()).build()
        onSuccess(mResponse)
        mBuffer.clear()
        eventListener?.callEnd(mCall)
        if (responseCallback != null) {
            try {
                responseCallback.onResponse(mCall, mResponse)
            } catch (e: IOException) {
                // Pass?
            }
        }
    }

    override fun onFailed(request: UrlRequest, info: UrlResponseInfo?, error: CronetException) {
        val e=error.asIOException()
        onError(e)
        mResponse = error.asIOException().okhttpResponse(mResponse)
        eventListener?.callFailed(mCall, e)
        responseCallback?.onFailure(mCall, e)


    }

    override fun onCanceled(request: UrlRequest, info: UrlResponseInfo) {
        super.onCanceled(request, info)
        onError( IOException("Request Canceled"))
        eventListener?.callEnd(mCall)
    }



    companion object {
        private const val MAX_FOLLOW_COUNT = 20
        private  val TAG = AbsCallBack::class.java.simpleName

        /**
         * 从UrlResponseInfo 中获取http协议
         *
         * @param responseInfo responseInfo
         * @return Protocol
         */
        private fun protocolFromNegotiatedProtocol(responseInfo: UrlResponseInfo): Protocol {
            val negotiatedProtocol = responseInfo.negotiatedProtocol.lowercase(Locale.getDefault())
            return when {
                negotiatedProtocol.contains("h3") -> {
                    Protocol.QUIC
                }
                negotiatedProtocol.contains("quic") -> {
                    Protocol.QUIC
                }
                negotiatedProtocol.contains("spdy") -> {
                    @Suppress("DEPRECATION")
                    Protocol.SPDY_3
                }
                negotiatedProtocol.contains("h2") -> {
                    Protocol.HTTP_2
                }
                negotiatedProtocol.contains("1.1") -> {
                    Protocol.HTTP_1_1
                }
                else -> {
                    Protocol.HTTP_1_0
                }
            }
        }


        private fun responseFromResponse(mCall: Call,response: Response, responseInfo: UrlResponseInfo): Response {
            val protocol = protocolFromNegotiatedProtocol(responseInfo)
            val headers = headersFromResponse(responseInfo)
            return response.newBuilder()
                .receivedResponseAtMillis(System.currentTimeMillis())
                .protocol(protocol)
                .request(mCall.request())
                .code(responseInfo.httpStatusCode)
                .message(responseInfo.httpStatusText)
                .headers(headers)
                .build()
        }

        private fun headersFromResponse(responseInfo: UrlResponseInfo): Headers {
            val headers = responseInfo.allHeadersAsList
            val headerBuilder = Headers.Builder()
            for ((key, value) in headers) {
                try {
                    if (key.equals("content-encoding", ignoreCase = true)) {
                        // Strip all content encoding headers as decoding is done handled by cronet
                        continue
                    }
                    headerBuilder.add(key, value)
                } catch (e: Exception) {
                    Log.w(TAG, "Invalid HTTP header/value: $key$value")
                    // Ignore that header
                }
            }
            return headerBuilder.build()
        }
    }

    init {
        mResponse = Response.Builder()
            .sentRequestAtMillis(System.currentTimeMillis())
            .request(mRequest)
            .protocol(Protocol.HTTP_1_0)
            .code(0)
            .message("")
            .build()
    }

    protected fun IOException.okhttpResponse(response: Response): Response {
        return response.newBuilder()
            .receivedResponseAtMillis(System.currentTimeMillis())
            .request(mCall.request())
            .code(404)
            .message(this.message.orEmpty())
            .build()
    }

    override fun close() {
        mBuffer.clear()
    }
}