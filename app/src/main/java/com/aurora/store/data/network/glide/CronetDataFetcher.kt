package com.aurora.store.data.network.glide

import android.util.Log
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.data.DataFetcher
import com.bumptech.glide.load.model.GlideUrl
import org.chromium.net.CronetEngine
import org.chromium.net.CronetException
import org.chromium.net.UrlRequest
import org.chromium.net.UrlResponseInfo
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.channels.Channels
import java.util.concurrent.Executors

class CronetDataFetcher<T>(engine: CronetEngine, var url: T) : UrlRequest.Callback(),
    DataFetcher<ByteBuffer>, AutoCloseable {
    private val bytesReceived = ByteArrayOutputStream()
    private val receiveChannel = Channels.newChannel(bytesReceived)

    private var dataCallback: DataFetcher.DataCallback<in ByteBuffer>? = null
    private var urlRequest: UrlRequest? = null
    private var builder: UrlRequest.Builder? = null
    override fun loadData(
        priority: Priority,
        dataCallback: DataFetcher.DataCallback<in ByteBuffer>
    ) {
        this.dataCallback = dataCallback
        builder!!.setPriority(CronetUrlLoaderFactory.GLIDE_TO_CHROMIUM_PRIORITY[priority]!!)
        builder!!.allowDirectExecutor()
        urlRequest = builder!!.build()
        if (url is GlideUrl) {
            val t = url as GlideUrl
            for ((key, value) in t.headers) {
                builder!!.addHeader(key, value)
            }
        }
        urlRequest?.start()
    }

    override fun cleanup() {
        bytesReceived.reset()
    }

    override fun cancel() {
        if (urlRequest != null) {
            urlRequest!!.cancel()
        }
    }

    override fun getDataClass(): Class<ByteBuffer> {
        return ByteBuffer::class.java
    }

    override fun getDataSource(): DataSource {
        return DataSource.REMOTE
    }

    @Throws(Exception::class)
    override fun onRedirectReceived(
        request: UrlRequest,
        info: UrlResponseInfo,
        newLocationUrl: String
    ) {
        request.followRedirect()
    }

    @Throws(Exception::class)
    override fun onResponseStarted(request: UrlRequest, info: UrlResponseInfo) {
        request.read(ByteBuffer.allocateDirect(BYTE_BUFFER_CAPACITY_BYTES))
    }

    @Throws(Exception::class)
    override fun onReadCompleted(
        request: UrlRequest,
        info: UrlResponseInfo,
        byteBuffer: ByteBuffer
    ) {
        byteBuffer.flip()
        try {
            receiveChannel.write(byteBuffer)
        } catch (e: IOException) {
            Log.i("CronetDataFetcher", "IOException during ByteBuffer read. Details: ", e)
        }
        //Reset the buffer to prepare it for the next read
        byteBuffer.clear()

        // Continue reading the request
        request.read(byteBuffer)
    }

    override fun onSucceeded(request: UrlRequest, info: UrlResponseInfo) {
        //Log.e("SSS",info.getAllHeadersAsList().toString());
        val byteBuffer = ByteBuffer.wrap(bytesReceived.toByteArray())
        dataCallback!!.onDataReady(byteBuffer)
    }

    override fun onFailed(request: UrlRequest, info: UrlResponseInfo, error: CronetException) {
        dataCallback!!.onLoadFailed(error)
    }

    @Throws(Exception::class)
    override fun close() {
    }

    companion object {
        private const val BYTE_BUFFER_CAPACITY_BYTES = 64 * 1024
        private val executor = Executors.newCachedThreadPool()
    }

    init {
        builder = if (url is GlideUrl) {
            val s = url as GlideUrl
            engine.newUrlRequestBuilder(s.toStringUrl(), this, executor)
        } else {
            engine.newUrlRequestBuilder(
                url as String,
                this,
                executor
            )
        }
    }
}