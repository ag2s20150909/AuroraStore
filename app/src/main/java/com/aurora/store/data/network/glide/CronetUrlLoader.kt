package com.aurora.store.data.network.glide

import com.bumptech.glide.load.Options
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoader.LoadData
import com.bumptech.glide.signature.ObjectKey
import org.chromium.net.CronetEngine
import java.nio.Buffer
import java.nio.ByteBuffer

class CronetUrlLoader<T : Any, B : Buffer> internal constructor(private val engine: CronetEngine) :
    ModelLoader<T, ByteBuffer> {
    override fun buildLoadData(
        model: T,
        width: Int,
        height: Int,
        options: Options
    ): LoadData<ByteBuffer> {
        return if (model is GlideUrl) {
            val t = model as GlideUrl
            LoadData(t,  /*fetcher=*/CronetDataFetcher(engine, t))
        } else {
            val s = model as String
            LoadData(ObjectKey(s), CronetDataFetcher(engine, s))
        }
    }

    override fun handles(url: T): Boolean {
        return if (url is GlideUrl) {
            true
        } else url.toString().startsWith("http://") || url.toString().startsWith("https://")
    }
}